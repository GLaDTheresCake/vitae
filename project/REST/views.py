from flask import Blueprint, abort, request, jsonify

from project import database

REST = Blueprint('REST', __name__, template_folder='templates')


@REST.route('/api/v1.1/collection', methods=['GET'])
def get_projects():
    """
    RESTful api service to query a database collection using a HTTP GET request.
    Example: localhost:5000/api/v1.1/collection?collection_name=projects would query the 'projects'
    collection in the database (replace localhost:5000 with the url and port of the website).
    If the returned value is None or has a length of 0, a 404 is thrown.
    :returns: A JSON formatted string is returned.
    """
    coll = database.get_collection(request.args.get('collection_name'))
    if coll is not None and len(coll) > 0:
        return jsonify(coll)
    else:
        abort(404)


@REST.route('/api/v1.1/entry', methods=['GET'])
def get_entry():
    e = request.args.copy().to_dict()
    e.pop('collection_name')
    entry = database.get_entry(request.args.get('collection_name'), e)
    if entry is not None and len(entry) > 0:
        return jsonify(entry)
    else:
        abort(404)
