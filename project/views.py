from flask import render_template, jsonify
from flask import make_response
from flask import request
from project.REST.views import REST
from project import app, database, cache
from werkzeug.exceptions import abort

app.register_blueprint(REST)


@app.route('/')
def index():
    """
    This is the default rendering page, it renders a list of the projects in the database with links to their pages.
    :return: Render the projects page containing a list of all projects
    """
    return render_template('hello.html', projects=database.get_collection('projects', fields={'name': 1, '_id': 0}))


@app.route('/hello')
@cache.cached(timeout=360)
def hello():
    return render_template('hello.html')


@app.route('/get_project/<string:name>')
@cache.cached(timeout=900)
def get_project(name):
    """
    This is the route for specific project pages, the url should look like this: <url>/get_project/<name> where <url> is
    the url of the website, and <name> is the name of the project.
    :param name: The name of the project in the database to render the page for.
    :return: Render the specific project page that corresponds to the name specified.
    """
    project = database.get_entry('projects', {'name': name})
    if project is not None:
        return render_template('project.html', project=project)
    abort(404)


@app.errorhandler(404)
def not_found(error):
    """
    This function handles 404 errors so that the api returns something friendly to api calls, and other urls return
    simple human readable 404s.
    :returns: This function returns a JSON formatted error message if the api is used, or a human friendly html file if
    another url is invoked.
    """
    if request.path.startswith("/api"):
        return make_response(jsonify({'error': 'Not found'}), 404)
    else:
        return render_template('404.html', url=str(request.url)), 404
