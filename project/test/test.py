from project import app, database
from flask_testing import TestCase


class ViewTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        return app

    def test_404_json(self):
        response = self.client.get("/api/v1.1/entry?collection_name=empty&name=collection")
        self.assert404(response)
        self.assertDictEqual({'error': 'Not found'}, response.json)

    def test_404_html(self):
        response = self.client.get("/notaurl")
        self.assert404(response)


class DatabaseTest(TestCase):
    def create_app(self):
        app.config['TESTING'] = True
        return app

    def setUp(self):
        database.add_collection('testing')
        database.add_entry('testing', args={'name': 'test'})

    def tearDown(self):
        database.remove_entry('testing', args={'name': 'test'})
        database.remove_collection('testing')

    def test_collection(self):
        response = self.client.get("/api/v1.1/collection?collection_name=testing")
        self.assertTrue(response.json[0]['name'] == 'test')

    def test_entry(self):
        response = self.client.get("/api/v1.1/entry?collection_name=testing&name=test")
        self.assertTrue(response.json['name'] == 'test')

    def test_project(self):
        response = self.client.get("/get_project/test")
        self.assert200(response)
