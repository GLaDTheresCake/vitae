from flask import Flask
from flask_cache import Cache

from project.database.database_data import database_name, database_ip, database_port
from project.database.mongo import mongo_database

app = Flask(__name__)
database = mongo_database(app, database_name, ip=database_ip, port=database_port)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

import project.views
