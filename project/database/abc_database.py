from abc import ABCMeta, abstractmethod

"""
The abstract database class.
abc_database is an abstract class explaining how to create a database driver for the application using templated code
used by this application.
If you want to create a new database class for a different database type, all you have to do is extend this class and
override its methods. An example fo this is the mongo.py class for the mongodb type of database.
"""


class abc_database:
    __metaclass__ = ABCMeta

    @abstractmethod
    def __init__(self, app=None):
        """
        :param app: the application context for the flask application, every database SHOULD use this, as you have to
        work within flask's application context to be able to do calls at any time.
        Initialize the drivers for the database, and process the data for the connection to the database here.
        """
        pass

    @abstractmethod
    def get_db(self):
        """
        Gets the database object created by the constructor.
        :return: database object of the given database type.
        """
        return

    @abstractmethod
    def get_collection(self, name, fields):
        # TODO: make the fields argument generic for all database types.
        """
        Gets all entries from a collection (or table if you use SQL) the database formatted in a list of json formatted
        files.
        :param fields: The specific fields to return from the query (excluding all the others).
        :param name: The name for the collection/table to search for.
        :returns task: A json-formatted text that has all the data for the collection/table in it.
        """
        return

    @abstractmethod
    def get_entry(self, name, args, fields):
        # TODO: make the fields argument generic for all database types.
        """
        Get an entry from the specified collection (or table if you use SQL) from the database given the conditions
        specified.
        :param fields: The specific fields to return from the query (excluding all the others).
        :param name: The name for the collection/table to search for.
        :param args: The attributes to search for in dict format, and their respective values to search for in the
        specified collection/table.
        :returns task: A json-formatted text that has all the data for the entry in it.
        """
        return

    @abstractmethod
    def add_collection(self, name, **kwargs):
        """
        Add a collection to the database with the name provided
        Use kwargs for adding different settings tot the collection.
        :param name: The name for the collection to add
        :return: True when the adding of the collection succeeds.
        """
        return

    @abstractmethod
    def add_entry(self, coll_name, args):
        """
        add an entry to the specified collection.
        Use args to add the attributes for the entry.
        :param args: Dictionary of attributes to add to the entry
        :param coll_name: the name of the collection to add the entry to
        :return:
        """
        return

    @abstractmethod
    def remove_entry(self, coll_name, args):
        """
        Remove any entries that match in the collection with name :coll_name: and with optional search arguments kwargs.
        :param coll_name: collection name to search in.
        :param args: Arguments to refine the search.
        :return: True when the operation is successful
        """
        return

    @abstractmethod
    def remove_collection(self, coll_name):
        """
        Remove the specified collection by collection name.
        :param coll_name: The name of the collection to be deleted.
        :return:
        """
        return
