from bson.json_util import dumps
from json import loads

from pymongo.errors import CollectionInvalid

from project.database.abc_database import abc_database
from flask_pymongo import PyMongo


class mongo_database(abc_database):

    mongo = None

    def __init__(self, app, database_name, ip='localhost', port=27017):
        super().__init__()
        self.mongo = PyMongo()
        app.config['MONGO_DBNAME'] = database_name
        app.config['MONGO_HOST'] = ip
        app.config['MONGO_PORT'] = port
        self.mongo.init_app(app)

    def get_db(self):
        return self.mongo.db

    def get_collection(self, name, args=None, fields=None):
        if args is None:
            args = {}
        return loads(dumps(list(self.get_db()[name].find(args, fields))))

    def get_entry(self, name, args, fields=None):
        return loads(dumps(self.get_db()[name].find_one(args, fields)))

    def add_collection(self, name, **kwargs):
        try:
            return self.get_db().create_collection(name, kwargs=kwargs) is not None
        except CollectionInvalid:
            return False

    def add_entry(self, coll_name, args):
        return self.get_db()[coll_name].insert_one(args) is not None

    def remove_collection(self, coll_name):
        self.get_db()[coll_name].drop()

    def remove_entry(self, coll_name, args):
        self.get_db()[coll_name].remove(args)
